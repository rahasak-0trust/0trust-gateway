package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
)

func postDevice() {
}

func postNotification() {
}

func post(data []byte, api string) (string, int) {
	req, err := http.NewRequest("POST", api, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")

	log.Printf("INFO: send request to, %s with %s", api, string(data))

	// send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("ERROR: fail request, %s", err.Error())
		return "Error internal service", 500
	}
	defer resp.Body.Close()
	b, _ := ioutil.ReadAll(resp.Body)

	log.Printf("INFO: got response %s, status %d", string(b), resp.StatusCode)

	return string(b), resp.StatusCode
}
